/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author dell
 */
public class PlayerUnitTest {

    public PlayerUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    public void testGetName() {
        Player o = new Player('O');
        assertEquals('O', o.getName());
    }

    public void testWin() throws Exception {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(1, table.getCurrentPlayer().getWin());
    }

    public void testDraw() throws Exception {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(0, 0);
        table.switchTurn();
        table.setRowCol(1, 0);
        table.switchTurn();
        table.setRowCol(2, 0);
        table.switchTurn();
        table.setRowCol(1, 1);
        table.switchTurn();
        table.setRowCol(1, 2);
        table.switchTurn();
        table.setRowCol(0, 1);
        table.switchTurn();
        table.setRowCol(2, 1);
        table.switchTurn();
        table.setRowCol(2, 2);
        table.switchTurn();
        table.setRowCol(0, 2);
        table.checkDraw();
        assertEquals(1, o.getDraw());
    }

    public void testLose() throws Exception {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        table.switchTurn();
        assertEquals(1, table.getCurrentPlayer().getLose());
    }
    
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
