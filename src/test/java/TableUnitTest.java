/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author dell
 */
public class TableUnitTest {

    public TableUnitTest() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }

    @Test
    public void testColWin() throws Exception {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        assertEquals(true, table.checkWin());
    }

    public void testRowWin() throws Exception {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        assertEquals(true, table.checkWin());
    }

    public void testX1Win() throws Exception {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o, x);
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        assertEquals(true, table.checkWin());
    }

    public void testX2Win() throws Exception {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(0, 2);
        table.setRowCol(1, 1);
        table.setRowCol(2, 0);
        assertEquals(true, table.checkWin());
    }

    public void testCheckDraw() throws Exception {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(0, 0);
        table.switchTurn();
        table.setRowCol(1, 0);
        table.switchTurn();
        table.setRowCol(2, 0);
        table.switchTurn();
        table.setRowCol(1, 1);
        table.switchTurn();
        table.setRowCol(1, 2);
        table.switchTurn();
        table.setRowCol(0, 1);
        table.switchTurn();
        table.setRowCol(2, 1);
        table.switchTurn();
        table.setRowCol(2, 2);
        table.switchTurn();
        table.setRowCol(0, 2);
        assertEquals(true, table.checkDraw());
    }

    public void testSwitchPlayer() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        if (table.getCurrentPlayer().getName() == 'O') {
            table.switchTurn();
            assertEquals('X', table.getCurrentPlayer().getName());
        } else {
            table.switchTurn();
            assertEquals('O', table.getCurrentPlayer().getName());
        }
    }

    public void testSetRowCol() throws Exception {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(0, 0);
        assertEquals(table.getCurrentPlayer().getName(), table.getData()[0][0]);
    }
    
    public void getWinner() throws Exception{
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(table.getCurrentPlayer(), table.getWinner());
    }
}
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}

